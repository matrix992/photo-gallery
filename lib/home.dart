import 'package:flutter/material.dart';
import 'space.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Images talk louder than words"),
        backgroundColor: Colors.red[400],
        centerTitle: true,
      ),
      body: GridView.count(
        crossAxisCount: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        children: [
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                FlatButton.icon(onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Space()));
                }, icon: Icon(Icons.assessment), label: Text("View"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo galler y mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo galler y mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo galler y mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo galler y mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo gallery mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          ),
          Card(
            elevation: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Expanded(child: Image.asset("images/a.jpg", width: 200,)),
                Text("this is a photo galler y mobile app"),
                ElevatedButton.icon(onPressed: (){}, icon: Icon(Icons.assessment), label: Text("add"))
              ],
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          print("I am clicked");
        },
        backgroundColor: Colors.red,
        child: Icon(Icons.assistant_navigation),
      ),
    );
  }
}




